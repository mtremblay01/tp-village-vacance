/*Section C.1
• Augmenter de 5,5% tous les tarifs des nuitées pour les villages de catégorie 1 et 2. */
UPDATE TARIF_NUITEE 
SET TARIF_UNITAIRE = (TARIF_UNITAIRE * 0.055) + TARIF_UNITAIRE
WHERE 
ID_CATEGORIE_VILLAGE BETWEEN 1 AND 2;
