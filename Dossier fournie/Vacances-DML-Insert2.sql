/* **********************************************************
	DML Insert - SCRIPT No 2
	Sch�ma MRD:	"Cas Village Vacances"
	Auteur:		Sylvie Monjal - C�gep de Ste-Foy  	
***********************************************************/

/* AVANT L'EX�CUTION DE CE SCRIPT No 2:
	- EX�CUTEZ LE SCRIPT "Vacances-DMLInsert1.sql"
*/

/* APR�S L'EX�CUTION DE CE SCRIPT No 2:
	- N'OUBLIEZ PAS DE FAIRE UN COMMIT SI TOUT A BIEN FONCTIONN�*/

/*===============================================================================
     Table CLIENT
/*===============================================================================*/	
SAVEPOINT CLIENTS;
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Daho',
		 '�tienne',
		 'M',
		 '(450)345-2511',
		 'Montr�al');

INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Fiset',
		 'Raymond',
		 'M',
		 '(514)345-6513',
		 '159, Av Turcotte, G1K 4X6, Montr�al');
INSERT INTO
    CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Gosselin',
		 'Yvonne',
		 'F',
		 '(418)688-4212',
		 '159, Rue Brown, Qu�bec');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Dupuis',
		 'Pierre',
		 'M',
		 '(514)345-2511',
		 'Des �rables, MONTR�AL');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Par�',
		 'Marine',
		 'F',
		 '(445)987-9351',
		 'Magog');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Caron',
		 'L�o',
		 'M',
		 '(514)412-2296',
		 '12, ROYALE, MONTREAL');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'St-Onge',
		 '�ric',
		 'M',
		 '(514)679-6600',
		 '181, St-Louis, Montr�al');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Plante',
		 'Jos�e',
		 'F',
		 '(514)236-5510',
		 '471, Veillon, Montr�al, Qc');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Fortin',
		 'Marine',
		 'F',
		 '(418)412-2296',
		 '412, 3Rue, QUEBEC');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Plante',
		 'Jos�e',
		 'F',
		 '(514)238-5510',
		 '471, Veillon, Montr�al, Qc');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'St-Onge',
		 '�ric',
		 'M',
		 '(514)412-2296',
		 '12, ROYALE, MONTREAL');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Fiset',
		 'Val�rie',
		 'F',
		 '(418)772-6453',
		 NULL);
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Roy',
		 'Paul',
		 'M',
		 '(514)772-6757',
		 '200, St-jean, montreal');
INSERT INTO
	CLIENT
		(ID_CLIENT,
		 NOM,
		 PRENOM,
		 SEXE,
		 TEL_DOMICILE,
		 ADRESSE)
	VALUES
		(SEQ_ID_CLIENT.NEXTVAL,
		 'Dallaire',
		 'Karine',
		 'F',
		 '(415)231-2512',
		 'm�gantic');
         
/*===============================================================================
     Table RESERVATION - Table SEJOUR
/*===============================================================================*/

SAVEPOINT RESERVATIONS;

-- == Village Casa-Dali == 
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 1: 5 nuits - 2 logements */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('12/02/2016','dd/mm/yyyy'),
		 1,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('15/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('15/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 109 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
         SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('16/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('16/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 109 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 109 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 109 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 109 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
-- fin r�servation
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 2: 6 nuits - 2 logements */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('13/02/2016','dd/mm/yyyy'),
		 8,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('13/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 18 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('13/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('14/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 18 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('14/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('15/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 18 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('15/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('16/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 18 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('16/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 18 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 18 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
-- fin r�servation
SAVEPOINT DEBUT_RES;
-- debut r�servation

SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 3: 4 nuits - 1 logement */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('15/02/2016','dd/mm/yyyy'),
		 7,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('9/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 3);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('10/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 3);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('11/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 3);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('12/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 3);
-- fin r�servation
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 4: 7 nuits - 5 logements */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('24/02/2016','dd/mm/yyyy'),
		 2,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
    SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 100 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 101 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 102 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 104 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 100 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 101 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 102 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 104 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 100 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 101 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 102 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 104 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 100 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 101 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 102 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 104 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('21/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 100 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('21/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 101 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('21/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 102 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('21/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 104 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('21/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('22/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 100 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('22/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 101 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('22/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 102 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('22/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 104 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('22/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('23/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 100 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('23/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 101 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('23/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 102 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('23/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 104 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('23/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
-- fin r�servation
SAVEPOINT DEBUT_RES;
/* r�servation 5: 6 nuits - 2 logements */
-- debut r�servation
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('19/02/2016','dd/mm/yyyy'),
		 5,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 103 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('21/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 103 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('21/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('22/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 103 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('22/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('23/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 103 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('23/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('24/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 103 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('24/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('25/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 103 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 5);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('25/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
-- fin r�servation
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 6: 4 nuits - 1 logement */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('31/01/2016','dd/mm/yyyy'),
		 12,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('06/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('07/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('08/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('09/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
-- fin r�servation
SAVEPOINT DEBUT_RES;
/* r�servation 7: 4 nuits - 1 logement+tous les logements de type C2 du village */
-- debut r�servation
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('12/12/2015','dd/mm/yyyy'),
		 9,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('26/03/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
            INNER JOIN TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
	WHERE
		VILLAGE.NOM_VILLAGE = 'Casa-Dali'
		AND
		TYPE_LOGEMENT.CODE_TYPE_LOGEMENT = 'C2';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('26/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('27/03/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
            INNER JOIN TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
	WHERE
		VILLAGE.NOM_VILLAGE = 'Casa-Dali'
		AND
		TYPE_LOGEMENT.CODE_TYPE_LOGEMENT = 'C2';
INSERT INTO
    SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('27/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('28/03/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
            INNER JOIN TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
	WHERE
		VILLAGE.NOM_VILLAGE = 'Casa-Dali'
		AND
		TYPE_LOGEMENT.CODE_TYPE_LOGEMENT = 'C2';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('28/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
         SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('29/03/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
            INNER JOIN TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
	WHERE
		VILLAGE.NOM_VILLAGE = 'Casa-Dali'
		AND
		TYPE_LOGEMENT.CODE_TYPE_LOGEMENT = 'C2';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('29/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 106 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
         SEQ_ID_RESERVATION.CURRVAL,
		 4);

-- fin r�servation
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 8: 3 nuits - tous les logements de type D2 du village */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('01/11/2015','dd/mm/yyyy'),
		 6,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('26/02/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		4
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
            INNER JOIN TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
	WHERE
		VILLAGE.NOM_VILLAGE = 'Casa-Dali'
		AND
		TYPE_LOGEMENT.CODE_TYPE_LOGEMENT = 'D2';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('27/02/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		4
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
            INNER JOIN TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
	WHERE
		VILLAGE.NOM_VILLAGE = 'Casa-Dali'
		AND
		TYPE_LOGEMENT.CODE_TYPE_LOGEMENT = 'D2';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('28/02/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		4
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
            INNER JOIN TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
	WHERE
		VILLAGE.NOM_VILLAGE = 'Casa-Dali'
		AND
		TYPE_LOGEMENT.CODE_TYPE_LOGEMENT = 'D2';

-- fin r�servation
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 9: 6 nuits - 1 logement */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('19/02/2016','dd/mm/yyyy'),
		 7,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('31/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 105 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
         SEQ_ID_RESERVATION.CURRVAL,
		 6);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('01/04/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 105 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 6);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('02/04/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 105 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 6);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('03/04/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 105 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 6);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('04/04/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 105 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 6);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('05/04/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 105 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 6);
-- fin r�servation
/* r�servation 10 : 2 nuits - 1 logement */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('31/01/2016','dd/mm/yyyy'),
		 12,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('03/04/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('04/04/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
-- fin r�servation */

SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 11: 37 nuits - 1 logement */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('02/01/2016','dd/mm/yyyy'),
		 14,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Casa-Dali'));

DECLARE
	DATE_SEJ DATE := TO_DATE('24/02/2016','dd/mm/yyyy');
BEGIN	
	FOR JOUR IN 1 .. (TO_DATE('02/04/2016','dd/mm/yyyy') - TO_DATE('24/02/2016','dd/mm/yyyy')) LOOP
		INSERT INTO
            SEJOUR
                (ID_SEJOUR,
                 DATE_SEJOUR,
                 ID_LOGEMENT,
                 ID_RESERVATION,
                 NB_PERSONNES)
            VALUES
                (SEQ_ID_SEJOUR.NEXTVAL,
                 DATE_SEJ,
                 (SELECT
                    ID_LOGEMENT
                  FROM
                    LOGEMENT
                        INNER JOIN VILLAGE
                            ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
                  WHERE
                    LOGEMENT.NO_LOGEMENT = 9 AND
                    VILLAGE.NOM_VILLAGE = 'Casa-Dali'),
                 SEQ_ID_RESERVATION.CURRVAL,
                 2);
		DATE_SEJ := DATE_SEJ + 1;
	END LOOP;
END;
/
-- fin r�servation

-- == Village Porto-Nuevo == 
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 12: 7 nuits - tous les logements village */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('15/09/2015','dd/mm/yyyy'),
		 1,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Porto-Nuevo'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('27/12/2015','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('28/12/2015','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('29/12/2015','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('30/12/2015','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('31/12/2015','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('01/01/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('02/01/2016','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
-- fin r�servation
SAVEPOINT DEBUT_RES;

-- debut r�servation
/* r�servation 13: 5 nuits - tous les logements village */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('17/02/2016','dd/mm/yyyy'),
		 3,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Porto-Nuevo'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('02/03/2017','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('03/03/2017','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('04/03/2017','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('05/03/2017','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	SELECT
		SEQ_ID_SEJOUR.NEXTVAL,
        TO_DATE('06/03/2017','dd/mm/yyyy'),
		LOGEMENT.ID_LOGEMENT,
		SEQ_ID_RESERVATION.CURRVAL,
		2
	FROM
		LOGEMENT
            INNER JOIN VILLAGE
                ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
	WHERE
		VILLAGE.NOM_VILLAGE = 'Porto-Nuevo';
-- fin r�servation
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 14:3 nuits - 1 logement */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('28/02/2016','dd/mm/yyyy'),
		 1,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Porto-Nuevo'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('07/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 1 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('08/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 1 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('09/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 1 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
-- fin r�servation
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 15: 6 nuits - 2 logements */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('28/02/2016','dd/mm/yyyy'),
		 8,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Porto-Nuevo'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('09/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 2 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('09/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 3 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('10/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 2 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('10/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 3 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('11/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 2 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('11/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 3 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);		 
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('12/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 2 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('12/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 3 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('13/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 2 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('13/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 3 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('14/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 2 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('14/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 3 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 2);		 
-- fin r�servation
----**********
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 16: 1 nuit - 1 logement */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('19/02/2016','dd/mm/yyyy'),
		 4,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Porto-Nuevo'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('01/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 108 AND
            VILLAGE.NOM_VILLAGE = 'Porto-Nuevo'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 3);
-- fin r�servation

-- == Village Kouros == 
SAVEPOINT DEBUT_RES;
-- debut r�servation
/* r�servation 17: 4 nuits - 1 logement */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('24/02/2016','dd/mm/yyyy'),
		 9,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Kouros'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('17/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 1);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 1);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 1);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 19 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 1);
-- fin r�servation
/* r�servation 18: 3 nuits - 2 logements */
INSERT INTO
	RESERVATION
		(ID_RESERVATION,
		 DATE_RESERVATION,
		 ID_CLIENT,
		 ID_VILLAGE)
	VALUES
		(SEQ_ID_RESERVATION.NEXTVAL,
		 TO_DATE('25/02/2016','dd/mm/yyyy'),
		 13,
		 (SELECT ID_VILLAGE FROM VILLAGE WHERE NOM_VILLAGE = 'Kouros'));
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 8 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 1);		 
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 8 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 1);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 8 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 1);	
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('18/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 107 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);		 
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('19/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 107 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);
INSERT INTO
	SEJOUR
		(ID_SEJOUR,
         DATE_SEJOUR,
		 ID_LOGEMENT,
		 ID_RESERVATION,
		 NB_PERSONNES)
	VALUES
		(SEQ_ID_SEJOUR.NEXTVAL,
         TO_DATE('20/03/2016','dd/mm/yyyy'),
        (SELECT
            ID_LOGEMENT
          FROM
            LOGEMENT
                INNER JOIN VILLAGE
                    ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
          WHERE
            LOGEMENT.NO_LOGEMENT = 107 AND
            VILLAGE.NOM_VILLAGE = 'Kouros'),
		 SEQ_ID_RESERVATION.CURRVAL,
		 4);			 
-- fin r�servation
----**********