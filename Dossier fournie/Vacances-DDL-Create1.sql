/* **********************************************************
	DDL de cr�ation des objets de la base de donn�es
	Sch�ma MRD         : "Villages Vacances" - Script no 1
	Par                : Sylvie Monjal
********************************************************** */

/* **********************************************************
	Table "CATEGORIE_VILLAGE"
********************************************************** */
CREATE TABLE CATEGORIE_VILLAGE (
	ID_CATEGORIE_VILLAGE    NUMERIC(1,0)    NOT NULL,  -- SEQ_ID_CATEGORIE_VILLAGE
	NO_CATEGORIE	        SMALLINT		NOT NULL,
	DESCRIPTION		        VARCHAR2(50)	NOT NULL,
	CONSTRAINT PK_CATEGORIE_VILLAGE
		PRIMARY KEY (ID_CATEGORIE_VILLAGE),
	CONSTRAINT U1_CATEGORIE_VILLAGE
		UNIQUE (NO_CATEGORIE),
	CONSTRAINT CHK_CAT_VILL_NO_CATEGORIE
		CHECK(NO_CATEGORIE BETWEEN 1 AND 5)
);
/* **********************************************************
	S�quence "SEQ_ID_CATEGORIE_VILLAGE"
********************************************************** */
CREATE SEQUENCE SEQ_ID_CATEGORIE_VILLAGE
	INCREMENT BY 1
	START WITH 1;
    
/* **********************************************************
	Table "TYPE_LOGEMENT"
********************************************************** */
CREATE TABLE TYPE_LOGEMENT(
	ID_TYPE_LOGEMENT        NUMERIC(2,0)	NOT NULL,  -- SEQ_ID_TYPE_LOGEMENT
	CODE_TYPE_LOGEMENT	    VARCHAR2(2)		NOT NULL,
	DESCRIPTION			    VARCHAR2(35)	NOT NULL,
	NB_MAX_PERSONNES	    SMALLINT		NOT NULL,
	CONSTRAINT PK_TYPE_LOGEMENT
		PRIMARY KEY (ID_TYPE_LOGEMENT),
	CONSTRAINT U1_TYPE_LOGEMENT
		UNIQUE (CODE_TYPE_LOGEMENT),
	CONSTRAINT CHK_TYPE_LOG_CODE	-- 1er caract�re=lettre, 2i�me caract�re=chiffre
		CHECK (SUBSTR(UPPER(CODE_TYPE_LOGEMENT), 1, 1) BETWEEN 'A' AND 'Z'   -- REGEXP_LIKE(CODE_TYPE_LOGEMENT, '[A-Z][1-9]')
			  AND
			  SUBSTR(CODE_TYPE_LOGEMENT, 2, 1) BETWEEN '1' AND '9'),
	CONSTRAINT CHK_TYPE_LOG_NB_MAX_PERS
		CHECK (NB_MAX_PERSONNES BETWEEN 1 AND 10)
);
/* **********************************************************
	S�quence "SEQ_ID_TYPE_LOGEMENT"
********************************************************** */
CREATE SEQUENCE SEQ_ID_TYPE_LOGEMENT
	INCREMENT BY 1
	START WITH 1;
    
/* **********************************************************
	Table "TARIF_NUITEE"
********************************************************** */
CREATE TABLE TARIF_NUITEE(
	ID_CATEGORIE_VILLAGE    NUMERIC(1,0)    NOT NULL,
	ID_TYPE_LOGEMENT        NUMERIC(2,0)	NOT NULL,
	TARIF_UNITAIRE		    NUMERIC(5,2)	NOT NULL,
	CONSTRAINT PK_TARIF_NUITEE_PRIM
		PRIMARY KEY (ID_CATEGORIE_VILLAGE, ID_TYPE_LOGEMENT),
	CONSTRAINT FK_TARIF_CATEG_VILLAGE
		FOREIGN KEY (ID_CATEGORIE_VILLAGE)
		REFERENCES CATEGORIE_VILLAGE (ID_CATEGORIE_VILLAGE),
	CONSTRAINT FK_TARIF_TYPE_LOG
		FOREIGN KEY (ID_TYPE_LOGEMENT)
		REFERENCES TYPE_LOGEMENT (ID_TYPE_LOGEMENT),
	CONSTRAINT CHK_TARIF_UNITAIRE
		CHECK (TARIF_UNITAIRE BETWEEN 20 AND 300)
);

/* **********************************************************
	Table "VILLAGE"
********************************************************** */
CREATE TABLE VILLAGE (
	ID_VILLAGE		        NUMERIC(2,0)	NOT NULL,  -- SEQ_ID_VILLAGE
	NOM_VILLAGE		        VARCHAR2(15)	NOT NULL,
	VILLE			        VARCHAR2(10)	NOT NULL,
	PAYS			        VARCHAR2(10)	NOT NULL,
	PRIX_TRANSPORT	        NUMERIC(6,2)	NOT NULL,
	ID_CATEGORIE_VILLAGE	SMALLINT		NOT NULL,
	CONSTRAINT PK_VILLAGE
		PRIMARY KEY (ID_VILLAGE),
	CONSTRAINT U1_VILLAGE
		UNIQUE (NOM_VILLAGE),
    CONSTRAINT FK_VILL_CATEGORIE_VILL
		FOREIGN KEY (ID_CATEGORIE_VILLAGE)
		REFERENCES CATEGORIE_VILLAGE(ID_CATEGORIE_VILLAGE),
	CONSTRAINT CHK_VILLAGE_PRIX_TRANSPORT
		CHECK(PRIX_TRANSPORT > 0)
);
/* **********************************************************
	S�quence "SEQ_ID_VILLAGE"
********************************************************** */
CREATE SEQUENCE SEQ_ID_VILLAGE
	INCREMENT BY 1
	START WITH 1;
    
/* **********************************************************
	Table "LOGEMENT"
********************************************************** */
CREATE TABLE LOGEMENT(
	ID_LOGEMENT             NUMERIC(4,0)	NOT NULL,  -- SEQ_ID_LOGEMENT
	NO_LOGEMENT			    SMALLINT		NOT NULL,
	ID_VILLAGE			    NUMERIC(2,0)	NOT NULL,
	ID_TYPE_LOGEMENT        NUMERIC(2,0)		NOT NULL,
	COMMENTAIRE			    VARCHAR2(75)	NULL,
	CONSTRAINT PK_LOGEMENT
		PRIMARY KEY (ID_LOGEMENT),
    CONSTRAINT U1_LOGEMENT
		UNIQUE (NO_LOGEMENT, ID_VILLAGE),
	CONSTRAINT FK_LOG_TYPE_LOGEMENT
		FOREIGN KEY (ID_TYPE_LOGEMENT)
		REFERENCES TYPE_LOGEMENT (ID_TYPE_LOGEMENT),
	CONSTRAINT FK_LOG_VILLAGE
		FOREIGN KEY (ID_VILLAGE)
		REFERENCES VILLAGE (ID_VILLAGE)
);
/* **********************************************************
	S�quence "SEQ_ID_LOGEMENT"
********************************************************** */
CREATE SEQUENCE SEQ_ID_LOGEMENT
	INCREMENT BY 1
	START WITH 1;
    
/* **********************************************************
	Table "CLIENT"
********************************************************** */
CREATE TABLE CLIENT(
	ID_CLIENT			NUMERIC(6,0)	NOT NULL,		-- SEQ_ID_CLIENT
	NOM					VARCHAR2(15)	NOT NULL,
	PRENOM				VARCHAR2(10)	NOT NULL,
	SEXE				CHAR(1)			NOT NULL,
	ADRESSE				VARCHAR2(50)	NULL,
	CONSTRAINT PK_CLIENT
		PRIMARY KEY (ID_CLIENT),
	CONSTRAINT CHK_CLIENT_SEXE
		CHECK(SEXE IN ('F','M'))
);
/* **********************************************************
	S�quence "SEQ_ID_CLIENT"
********************************************************** */
CREATE SEQUENCE SEQ_ID_CLIENT
	INCREMENT BY 1
	START WITH 1;

/* **********************************************************
	Table "RESERVATION"
********************************************************** */
CREATE TABLE RESERVATION(
	ID_RESERVATION		NUMERIC(7,0)			NOT NULL,		-- SEQ_ID_RESERVATION
	DATE_RESERVATION	DATE DEFAULT SYSDATE	NOT NULL ,
	ID_CLIENT			NUMERIC(6,0)			NOT NULL,
	ID_VILLAGE			NUMERIC(2,0)			NOT NULL,
	CONSTRAINT PK_RESERVATION
		PRIMARY KEY (ID_RESERVATION),
	CONSTRAINT FK_RES_CLIENT
		FOREIGN KEY (ID_CLIENT)
		REFERENCES CLIENT (ID_CLIENT),
	CONSTRAINT FK_RES_VILLAGE
		FOREIGN KEY (ID_VILLAGE)
		REFERENCES VILLAGE (ID_VILLAGE)
);
/* **********************************************************
	S�quence "SEQ_ID_RESERVATION"
********************************************************** */
CREATE SEQUENCE SEQ_ID_RESERVATION
	INCREMENT BY 1
	START WITH 1000;