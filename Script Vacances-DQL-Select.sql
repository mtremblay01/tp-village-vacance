/*=========================================================================

Question B.1 - 5 points
• Produire la liste des tarifs des nuitées pour le type de logement D3.
• Pour chaque prix, indiquer dans l’ordre :
    - code du type de logement,
    - description du type de logement,
    - numéro de la catégorie du village,
    - description de la catégorie du village,
    - prix/nuit/personne en $ canadiens (format affichage : 50.00 $Can).
• Trier par catégorie de village.

==========================================================================*/

SELECT
	TARIF.ID_TYPE_LOGEMENT,
	TYPE_LO.DESCRIPTION,
	TARIF.ID_CATEGORIE_VILLAGE,
	CAT.DESCRIPTION,
	TARIF.TARIF_UNITAIRE
FROM
	TARIF_NUITEE TARIF
		INNER JOIN TYPE_LOGEMENT TYPE_LO
			ON TARIF.ID_TYPE_LOGEMENT = TYPE_LO.ID_TYPE_LOGEMENT
		INNER JOIN CATEGORIE_VILLAGE CAT
			ON TARIF.ID_CATEGORIE_VILLAGE = CAT.ID_CATEGORIE_VILLAGE
WHERE
	TYPE_LO.CODE_TYPE_LOGEMENT = 'D3'
ORDER BY
	TARIF.ID_CATEGORIE_VILLAGE DESC;

/*=========================================================================

Question B.2

• Produire la liste des types de logement proposés dans le village Kouros.
• Pour chaque type de logement, indiquer dans l’ordre :
    - le code du type de logement,
    - la description du type de logement.
• Trier par code de type de logement.

==========================================================================*/

SELECT
    TYPE_LOGEMENT.CODE_TYPE_LOGEMENT,
    TYPE_LOGEMENT.DESCRIPTION
FROM
    VILLAGE
    INNER JOIN
        LOGEMENT
            ON LOGEMENT.ID_VILLAGE = VILLAGE.ID_VILLAGE
    INNER JOIN
        TYPE_LOGEMENT
            ON TYPE_LOGEMENT.ID_TYPE_LOGEMENT = LOGEMENT.ID_TYPE_LOGEMENT
WHERE
    NOM_VILLAGE = 'Kouros'
ORDER BY
    TYPE_LOGEMENT.CODE_TYPE_LOGEMENT;


/*=========================================================================

Question B.3

• Calculer le tarif moyen des nuitées pour chaque catégorie de village.
• Pour chaque catégorie de village, indiquer dans l’ordre :
    - numéro de la catégorie du village,
    - description de la catégorie du village,
    - prix moyen par personne et par nuit des logements (format affichage : 43.64 $Can).
• Trier par catégorie de village.

==========================================================================*/

SELECT
    CATEGORIE_VILLAGE.NO_CATEGORIE,
    CATEGORIE_VILLAGE.DESCRIPTION,
    AVG(TARIF_NUITEE.TARIF_UNITAIRE) AS MOYENNE_COUT_PAR_NUITEE
FROM
    CATEGORIE_VILLAGE
        INNER JOIN TARIF_NUITEE
            ON CATEGORIE_VILLAGE.ID_CATEGORIE_VILLAGE = TARIF_NUITEE.ID_CATEGORIE_VILLAGE
GROUP BY
    CATEGORIE_VILLAGE.NO_CATEGORIE,
    CATEGORIE_VILLAGE.DESCRIPTION;

/*=========================================================================

Question B.4 - 5 points
• Produire le calendrier d’occupation du logement 108 du village Casa-Dali pour le mois de mars 2016.
• Indiquer dans l’ordre :
    - numéro du logement,
    - nom du village,
    - nom du pays,
    - code du type de logement,
    - description du type de logement,
    - numéro de la réservation,
    - date du séjour (de la nuit occupée) (format affichage : jj/mm/aaaa).
• Trier par date(s).

==========================================================================*/

SELECT
	LOGEMENT.NO_LOGEMENT,
	VIL.NOM_VILLAGE,
	VIL.PAYS,
	TYPE_LO.CODE_TYPE_LOGEMENT,
	TYPE_LO.DESCRIPTION,
	SEJ.ID_RESERVATION,
	SEJ.DATE_SEJOUR
FROM
	LOGEMENT LOGEMENT
		INNER JOIN VILLAGE VIL
			ON LOGEMENT.ID_VILLAGE = VIL.ID_VILLAGE
		INNER JOIN TYPE_LOGEMENT TYPE_LO
			ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LO.ID_TYPE_LOGEMENT,
	SEJOUR SEJ
		INNER JOIN RESERVATION RES
			ON SEJ.ID_RESERVATION = RES.ID_RESERVATION
WHERE
	SEJ.DATE_SEJOUR BETWEEN '2016-03-01' AND '2016-03-30'and
	LOGEMENT.NO_LOGEMENT = 108 and
	VIL.NOM_VILLAGE = 'Casa-Dali'
ORDER BY
	SEJ.DATE_SEJOUR;

/*=========================================================================
Question B.5 - 9 points
• Produire les confirmations pour toutes les réservations effectuées (date de réservation) entre le 12 et
  le 20 février 2016 inclusivement.
• Pour chaque réservation, indiquer dans l’ordre :
    - numéro de la réservation,
    - nom du village,
    - date de départ de Montréal (format affichage : jj/mm/aaaa),
    - date de retour de Montréal (format affichage : jj/mm/aaaa),
    - nombre total de personnes concernées par la réservation,
    - numéro du client,
    - nom du client,
    - prénom du client.
• Trier par date de réservation, puis par numéro de réservation.

==========================================================================*/

SELECT
	RES.ID_RESERVATION,
	VIL.NOM_VILLAGE,
	TO_CHAR(MIN(SEJ.DATE_SEJOUR), 'dd/mm/yyyy') AS "DATE_DEBUT",
	TO_CHAR(MAX(SEJ.DATE_SEJOUR + 1), 'dd/mm/yyyy') AS "DATE_RETOUR",
	(SUM(SEJ.NB_PERSONNES) / COUNT(DISTINCT SEJ.DATE_SEJOUR)),
	CLI.NOM,
	CLI.PRENOM
FROM
	RESERVATION RES
		INNER JOIN VILLAGE VIL
			ON RES.ID_VILLAGE = VIL.ID_VILLAGE
		INNER JOIN CLIENT CLI
			ON RES.ID_CLIENT = CLI.ID_CLIENT
		INNER JOIN SEJOUR SEJ
			ON RES.ID_RESERVATION = SEJ.ID_RESERVATION
WHERE
	RES.DATE_RESERVATION BETWEEN '2016-02-12' AND '2016-02-20'
GROUP BY
	RES.ID_RESERVATION,
	VIL.NOM_VILLAGE,
	RES.ID_CLIENT,
	CLI.NOM,
	CLI.PRENOM,
	RES.DATE_RESERVATION
ORDER BY
	RES.DATE_RESERVATION,
	ID_RESERVATION;

/*=========================================================================

Question B.6 - 8 points
• Produire la liste des logements du village Casa-Dali disponibles pour toute la période du 17 au 23 mars
  2016 inclusivement.
• Pour chaque logement disponible, indiquer dans l’ordre :
    - numéro du logement,
    - code du type de logement,
    - description du type de logement.
• Trier par logement.

==========================================================================*/
SELECT
	NO_LOGEMENT,
	CODE_TYPE_LOGEMENT,
	DESCRIPTION
FROM
	LOGEMENT LO
		INNER JOIN TYPE_LOGEMENT TYPE_LO
			ON LO.ID_TYPE_LOGEMENT = TYPE_LO.ID_TYPE_LOGEMENT
		INNER JOIN VILLAGE VIL
			ON LO.ID_VILLAGE = VIL.ID_VILLAGE
		INNER JOIN SEJOUR SEJ
			ON LO.ID_LOGEMENT = SEJ.ID_LOGEMENT
WHERE
	VIL.NOM_VILLAGE = 'Casa-Dali' and
	(LO.ID_LOGEMENT) NOT IN (SELECT
								SEJ.ID_LOGEMENT
							FROM
								SEJOUR SEJ
							WHERE SEJ.DATE_SEJOUR BETWEEN '2016-03-17' AND '2016-03-23')
ORDER BY
	LO.ID_LOGEMENT;




/*=========================================================================

Question B.7 - 8 points
• Produire un rapport sur le taux d’occupation des logements (nombre de nuits où le logement est
  occupé sur une période) du village Casa-Dali pour la période du 7 mars au 23 mars 2016 inclusivement.
• Pour chaque logement du village dont le taux d’occupation est inférieur à 30%, indiquer dans l’ordre :
    - taux d’occupation (format affichage : 24%),
    - numéro du logement,
    - code du type de logement,
    - description du type de logement.
• Trier par taux d’occupation.

==========================================================================*/

SELECT
    TRUNC(COUNT(SEJOUR.ID_SEJOUR) / 17 * 100) || '%' AS TAUX_OCCUPATION,
    LOGEMENT.NO_LOGEMENT,
    TYPE_LOGEMENT.CODE_TYPE_LOGEMENT,
    TYPE_LOGEMENT.DESCRIPTION
FROM
    VILLAGE
        INNER JOIN
            LOGEMENT
                ON VILLAGE.ID_VILLAGE = LOGEMENT.ID_VILLAGE
        INNER JOIN
            TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
        INNER JOIN
            SEJOUR
                ON LOGEMENT.ID_LOGEMENT = SEJOUR.ID_LOGEMENT
WHERE
    NOM_VILLAGE = 'Casa-Dali'
    AND
    SEJOUR.DATE_SEJOUR BETWEEN '2016-03-07' AND '2016-03-23'
GROUP BY
    LOGEMENT.NO_LOGEMENT,
    TYPE_LOGEMENT.CODE_TYPE_LOGEMENT,
    TYPE_LOGEMENT.DESCRIPTION;

/*=========================================================================

Question B.8
• Quel ou quels sont le ou les villages avec le plus grand nombre de nuitées vendues pour le mois de mars 2016?
• Indiquer dans l’ordre :
    - pays,
    - nom village,
    - nombre de nuitées.

==========================================================================*/

SELECT
    VILLAGE.NOM_VILLAGE,
    VILLAGE.PAYS,
    COUNT(SEJOUR.ID_SEJOUR)
FROM
    VILLAGE
        INNER JOIN
            LOGEMENT
                ON VILLAGE.ID_VILLAGE = LOGEMENT.ID_VILLAGE
        INNER JOIN
            TYPE_LOGEMENT
                ON LOGEMENT.ID_TYPE_LOGEMENT = TYPE_LOGEMENT.ID_TYPE_LOGEMENT
        INNER JOIN
            SEJOUR
                ON LOGEMENT.ID_LOGEMENT = SEJOUR.ID_LOGEMENT
WHERE
    SEJOUR.DATE_SEJOUR BETWEEN '2016-03-01' AND '2016-03-31'
HAVING
    COUNT(SEJOUR.ID_SEJOUR) = ( SELECT
                                    MAX(COUNT(SEJOUR.ID_SEJOUR))
                                FROM
                                    VILLAGE
                                        INNER JOIN
                                            LOGEMENT
                                                ON VILLAGE.ID_VILLAGE = LOGEMENT.ID_VILLAGE
                                        INNER JOIN
                                            SEJOUR
                                                ON LOGEMENT.ID_LOGEMENT = SEJOUR.ID_LOGEMENT
                                WHERE
                                    SEJOUR.DATE_SEJOUR BETWEEN '2016-03-01' AND '2016-03-31'
                                GROUP BY
                                    VILLAGE.NOM_VILLAGE,
                                    VILLAGE.PAYS)
GROUP BY
    VILLAGE.NOM_VILLAGE,
    VILLAGE.PAYS;

/*=========================================================================

Question B.9 - 10 points
• Calculer le montant total hors taxes à facturer pour les réservations effectuées au mois de février 2016 (date de la réservation).
  Le montant facturé est calculé ainsi :
  (Prix transport/personne * nombre total personnes) + Σ [pour chaque logement] (nombre nuits * nombre personnes * tarif/nuit/personne)
  Ainsi, le montant facturé pour la réservation no 2, 5 nuits et 2 logements (108 et 109) dans le village Casa-Dali, est de 7404 $.
• Pour chaque réservation, indiquer dans l’ordre :
    - numéro de la réservation,
    - date de la réservation (format affichage : jj/mm/aaaa),
    - montant hors taxes à facturer (format affichage : 2138.00 $Can),
    - nom du pays,
    - nom du village.
• Trier par réservation.

Formule :
(Prix transport/personne * nombre total personnes) + Σ [pour chaque logement] (nombre nuits * nombre personnes * tarif/nuit/personne)
==========================================================================*/

SELECT
    RESERVATION.ID_RESERVATION,
    RESERVATION.DATE_RESERVATION,
    (AVG(VILLAGE.PRIX_TRANSPORT) * (SUM(SEJOUR.NB_PERSONNES) / COUNT(DISTINCT SEJOUR.DATE_SEJOUR))) + (COUNT(DISTINCT SEJOUR.DATE_SEJOUR) * (SUM(SEJOUR.NB_PERSONNES) / COUNT(DISTINCT SEJOUR.DATE_SEJOUR)) * AVG(TARIF_NUITEE.TARIF_UNITAIRE)) || '$' AS PRIX_TOTAL,
    VILLAGE.PAYS,
    VILLAGE.NOM_VILLAGE
FROM
    RESERVATION
        INNER JOIN SEJOUR
            ON RESERVATION.ID_RESERVATION = SEJOUR.ID_RESERVATION
        INNER JOIN VILLAGE
            ON RESERVATION.ID_VILLAGE = VILLAGE.ID_VILLAGE
        INNER JOIN LOGEMENT
            ON SEJOUR.ID_LOGEMENT = LOGEMENT.ID_LOGEMENT
        INNER JOIN TARIF_NUITEE
            ON LOGEMENT.ID_TYPE_LOGEMENT = TARIF_NUITEE.ID_TYPE_LOGEMENT
               AND VILLAGE.ID_CATEGORIE_VILLAGE = TARIF_NUITEE.ID_CATEGORIE_VILLAGE
GROUP BY
    RESERVATION.ID_RESERVATION,
    RESERVATION.DATE_RESERVATION,
    VILLAGE.PAYS,
    VILLAGE.NOM_VILLAGE
ORDER BY
    RESERVATION.ID_RESERVATION;


